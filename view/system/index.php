<?php
  session_start();
  $disable = "disabled";
  unset($_SESSION['msgd']);

    if ($_SESSION['usr_name']=="") {
        header("Location: /view/login/");
    }

  if ($_SESSION['role']=="admin") {
      $disable = "";
  }



  $db = new PDO("mysql:host=localhost;dbname=isw613_phonedirectory;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


  // v3
  $office = $db->prepare("select * from offices;");
  $office->execute(array());

  $user = $db->prepare("select * from users;");
  $user->execute(array());

  $department = $db->prepare("select * from departments;");
  $department->execute(array());

  $person = $db->prepare("select * from persons;");
  $person->execute(array());

  $phone = $db->prepare("select * from phones_numbers;");
  $phone->execute(array());
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AvanSoftware System</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body style="text-align: justify; text-justify: inter-word;">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="">AvanSoftware</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <?php  ?>
            <li class="nav-item active">
              <a class="nav-link" href="">Phone's number directory</a>
            </li>
          </ul>
          <div class="">
            <a class="btn btn-outline-warning btn-sm" href="/util/validation/logout.php"> Log out </a>
          </div>
        </div>
      </nav>
      <br><br>
      <div class="container">
        <div class="row">
          <br>
          <div class="col-md-2">

          </div>
          <div class="col-md-8">
            <br>
            <?php if (isset($_SESSION['msgl'])) {?>
              <div class="alert alert-warning" role="alert"><?php echo $_SESSION['msg']; ?></div>
            <?php } ?>
            <br>
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?= $disable ?>" id="user-tab" data-toggle="tab" href="#user" role="tab" aria-controls="user" aria-selected="true">Users</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= $disable ?>" id="office-tab" data-toggle="tab" href="#office" role="tab" aria-controls="office" aria-selected="false">Offices</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= $disable ?>" id="department-tab" data-toggle="tab" href="#department" role="tab" aria-controls="department" aria-selected="false">Departments</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link <?= $disable ?>" id="person-tab" data-toggle="tab" href="#person" role="tab" aria-controls="person" aria-selected="false">Persons</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $disable ?>" id="phone-tab" data-toggle="tab" href="#phone" role="tab" aria-controls="phone" aria-selected="false">Phones numbers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="phone" aria-selected="false">Search</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="user" role="tabpanel" aria-labelledby="user-tab">
                  <br>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-8">
                          <div class="bg-dark card text-center">
                            <div class="card-body">
                              <h5 style="color: #fff;">User <a class="btn btn-outline-warning" href="/view/crud/users.php">Edit</a></h5>
                              <form role="form" class="login-form" action="/util/validation/insert.php" name="insert_user" method="post">
                                <input type="hidden" name="form" value="insert_user">
                                <div class="form-group">
                                  <br>
                                  <label style="color: #fff;" for="sel1">Select to update</label>
                                  <select class="form-control" name="user_id">
                                    <option name="user_id" value="0">New</option>
                                    <?php while ($rowU = $user->fetch(PDO::FETCH_ASSOC)) {?>
                                      <option name="user_id" value="<?php echo $rowU['id']; ?>"><?php echo $rowU['full_name']; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon1">*</span>
                                  <input type="text" id="full_name" class="form-control" name="full_name" placeholder="Fullname" aria-label="Fullname" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">*</span>
                                  <input type="text" id="usr_name" class="form-control" name="usr_name" placeholder="Username" aria-label="Username" aria-describedby="basic-addon2">
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">*</span>
                                  <input type="password" id="password" class="form-control" name="password" placeholder="Password" aria-label="Pasword" aria-describedby="basic-addon2">
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">*</span>
                                  <input type="password" id="ver_password" class="form-control" name="ver_password" placeholder="Verificar Password" aria-label="Pasword" aria-describedby="basic-addon2">
                                </div>
                                <br>
                                <div class="btn-group" data-toggle="buttons">
                                  <label class="btn type btn-warning active">
                                    <input type="radio" name="options" id="option1" value="user" autocomplete="off" checked> Basic user
                                  </label>
                                  <label class="btn type btn-warning">
                                    <input type="radio" name="options" id="option2" value="admin" autocomplete="off"> Admin
                                  </label>
                                </div>
                                <br>
                                <br>
                                <button type="submit" class="btn btn-outline-warning">Insert</button>
                              </form>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-2">

                      </div>
                    </div>
                    <br>
                  </div>
                </div>
                <div class="tab-pane fade" id="office" role="tabpanel" aria-labelledby="office-tab">
                  <br>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-8">
                          <div class="bg-dark card text-center">
                            <div class="card-body">
                              <h5 style="color: #fff;">Office <a class="btn btn-outline-warning" href="/view/crud/offices.php">Edit</a> </h5>
                              <form class="login-form" action="/util/validation/insert.php" name="insert_office" method="POST">
                                <input type="hidden" name="form" value="insert_office">
                                <div class="form-group">
                                  <br>
                                  <label style="color: #fff;" for="sel1">Select to update</label>
                                  <select class="form-control" name="office_id">
                                    <option name="office_id" value="0">New</option>
                                    <?php while ($rowO = $office->fetch(PDO::FETCH_ASSOC)) {?>
                                      <option name="office_id" value="<?php echo $rowO['id']; ?>"><?php echo $rowO['office_name']; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon1">*</span>
                                  <input type="text" id="office_name" class="form-control" name="office_name" placeholder="Office name" aria-label="Oficce_name" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <button type="submit" class="btn btn-outline-warning">Insert</button>
                              </form>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-2">

                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="department" role="tabpanel" aria-labelledby="department-tab">
                  <br>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-8">
                          <div class="bg-dark card text-center">
                            <div class="card-body">
                              <h5 style="color: #fff;">Department <a class="btn btn-outline-warning" href="/view/crud/department.php">Edit</a></h5>
                              <form class="login-form" action="/util/validation/insert.php" name="insert_department" method="POST">
                                <input type="hidden" name="form" value="insert_department">
                                <div class="form-group">
                                  <br>
                                  <label style="color: #fff;" for="sel1">Select to update</label>
                                  <select class="form-control" name="dep_id">
                                    <option name="dep_id" value="0">New</option>
                                    <?php $department->execute(array());  while ($rowD = $department->fetch(PDO::FETCH_ASSOC)) {?>
                                      <option name="dep_id" value="<?php echo $rowD['id']; ?>"><?php echo $rowD['department_name']; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon1">*</span>
                                  <input type="text" id="department_name" class="form-control" name="department_name" placeholder="Department name" aria-label="Department_name" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                  <div class="form-group">
                                    <label style="color: #fff;" for="sel1">Select the office name:</label>
                                    <select class="form-control" name="office_id">
                                      <?php $office->execute(array()); while ($row = $office->fetch(PDO::FETCH_ASSOC)) {?>
                                        <option name="office_id" value="<?php echo $row['id']; ?>"><?php echo $row['office_name']; ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                <br>
                                <button type="submit" class="btn btn-outline-warning">Insert</button>
                              </form>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-2">

                      </div>
                    </div>
                    <br>
                  </div>
                </div>
                <div class="tab-pane fade" id="person" role="tabpanel" aria-labelledby="person-tab">
                  <br>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-2">

                      </div>
                      <div class="col-md-8">
                          <div class="bg-dark card text-center">
                            <div class="card-body">
                              <h5 style="color: #fff;">Person <a class="btn btn-outline-warning" href="/view/crud/persons.php">Edit</a></h5>
                              <form class="login-form" action="/util/validation/insert.php" name="insert_person" method="POST">
                                <input type="hidden" name="form" value="insert_person">
                                <div class="form-group">
                                  <br>
                                  <label style="color: #fff;" for="sel1">Select to update</label>
                                  <select class="form-control" name="per_id">
                                    <option name="per_id" value="0">New</option>
                                    <?php while ($rowP = $person->fetch(PDO::FETCH_ASSOC)) {?>
                                      <option name="per_id" value="<?php echo $rowP['id']; ?>"><?php echo $rowP['first_name']; ?> <?php echo $rowP['last_name']; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon1">*</span>
                                  <input type="text" id="first_name" class="form-control" name="first_name" placeholder="First name" aria-label="Firstname" aria-describedby="basic-addon1">
                                </div>
                                <br>
                                <div class="input-group">
                                  <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">*</span>
                                  <input type="text" id="last_name" class="form-control" name="last_name" placeholder="Lastname" aria-label="Lastname" aria-describedby="basic-addon2">
                                </div>
                                <br>
                                <div class="form-group">
                                  <label style="color: #fff;" for="sel1">Select the department:</label>
                                  <select class="form-control" name="department_id">
                                    <?php $department->execute(array()); while ($row = $department->fetch(PDO::FETCH_ASSOC)) {?>
                                      <option name="department_id" value="<?php echo $row['id']; ?>"><?php echo $row['department_name']; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-outline-warning">Insert</button>
                              </form>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-2">

                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="phone" role="tabpanel" aria-labelledby="phone-tab">
                  <br>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-6">
                        <div class="bg-dark card text-center">
                          <div class="card-body">
                            <h5 style="color: #fff;">Phone's numbers <a class="btn btn-outline-warning" href="/view/crud/phones.php">Edit</a></h5>
                            <form class="login-form" action="/util/validation/insert.php" name="insert_person" method="POST">
                              <input type="hidden" name="form" value="insert_phone">
                              <div class="form-group">
                                <br>
                                <label style="color: #fff;" for="sel1">Select to update</label>
                                <select class="form-control" name="pho_id">
                                  <option name="pho_id" value="0">New</option>
                                  <?php while ($rowP = $phone->fetch(PDO::FETCH_ASSOC)) {?>
                                    <option name="pho_id" value="<?php echo $rowP['id']; ?>"><?php echo $rowP['phone_number']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <label style="color: #fff;" for="sel1">Select the type:</label>
                                <select class="form-control" name="type">
                                    <option name="type" value="work">Work</option>
                                    <option name="type" value="home">Home</option>
                                    <option name="type" value="mobile">Mobile</option>
                                </select>
                              </div>
                              <div class="input-group">
                                <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon1">*</span>
                                <input onkeypress="return justNumbers(event);" type="text" id="extension" class="form-control" name="extension" placeholder="Extension" aria-label="Extension" aria-describedby="basic-addon1">
                              </div>
                              <br>
                              <div class="input-group">
                                <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">*</span>
                                <input onkeypress="return justNumbers(event);" type="text" id="phone_number" class="form-control" name="phone_number" placeholder="Phone number" aria-label="Phone number" aria-describedby="basic-addon2">
                              </div>
                              <br>
                              <div class="input-group">
                                <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">*</span>
                                <input type="text" id="annotation" class="form-control" name="annotation" placeholder="Annotation" aria-label="Annotation" aria-describedby="basic-addon2">
                              </div>
                              <br>
                              <div class="form-group">
                                <label style="color: #fff;" for="sel1">Select the person:</label>
                                <select class="form-control" name="person_id">
                                  <?php $person->execute(array()); while ($row = $person->fetch(PDO::FETCH_ASSOC)) {?>
                                    <option name="department_id" value="<?php echo $row['id']; ?>"><?php echo $row['first_name']; ?> <?php echo $row['last_name']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                              <br>
                              <button type="submit" class="btn btn-outline-warning">Insert</button>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">

                      </div>
                    </div>
                    <br>
                  </div>
                </div>
                <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="phone-tab">
                  <br>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="bg-dark card text-center">
                          <div class="card-body">
                            <h5 style="color: #fff;">Phone's list</h5>
                            <form class="" action="/view/crud/search.php" name="search_form" method="post">
                              <div class="container">
                                <div class="row">
                                  <div class="col-md-4">

                                </div>
                                  <div class="col-md-4">
                                    <input type="hidden" name="search_form" value="send">
                                    <br>
                                    <button type="submit" class="btn btn-outline-warning">Search</button>
                                    <br>
                                  </div>
                                  <div class="col-md-4">

                                </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-2">

          </div>
        </div>
      </div>
      <script type="text/javascript">
      function justNumbers(e)
        {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
        return true;

        return /\d/.test(String.fromCharCode(keynum));
        }
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
