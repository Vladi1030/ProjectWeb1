<?php
  $db = new PDO("mysql:host=localhost;dbname=isw613_phonedirectory;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  // when binding parameters you must pass a variable by reference
  // you cannot bind a literal
  session_start();
  unset($_SESSION['msg']);

  if ($_SESSION['usr_name']=="") {
      header("Location: /view/login/");
  }

  /*if ($_POST['doit']!="") {
    $id = $_POST['idU'];
    $name = $_POST['name'];
    echo "UPDATE offices SET office_name = ". $name ." WHERE id = ". $id;
    $update = $db->prepare("UPDATE offices SET office_name = ". $name ." WHERE id = ". $id .";");
    $update->execute();
  }*/

  $stmt = $db->prepare("SELECT * FROM offices;");
  $stmt->execute(array());
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AvanSoftware System</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body style="text-align: justify; text-justify: inter-word;">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand
        " href="">AvanSoftware</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="">Phone's number directory</a>
            </li>
          </ul>
          <div class="">
            <a class="btn btn-outline-warning btn-sm" href="/view/system/"> Back </a>
          </div>
        </div>
      </nav>
      <br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <?php if (isset($_SESSION['msgd'])) {?>
              <div class="alert alert-warning" role="alert"><?php echo $_SESSION['msgd']; ?></div>
            <?php } ?>
            <br>
            <table class="table">
              <thead class="thead-inverse">
                <tr>
                  <th></th>
                  <th>Office name</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {?>
                    <tr>
                        <td scope="row"></td>
                          <td><?php echo $row['office_name']; ?></td>
                        <td>
                          <form class="" action="/util/validation/delete.php" method="post">
                            <input type="hidden" name="crud_type" value="off">
                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                            <button class="btn btn-outline-danger btn-sm" type="submit" name="button">Delete</button>
                          </form>
                        </td>
                    </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </body>
</html>
