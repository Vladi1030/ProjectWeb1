<?php

session_start();
unset($_SESSION['msg']);
if ($_SESSION['usr_name']=="") {
    header("Location: /view/login/");
}

$db = new PDO("mysql:host=localhost;dbname=isw613_phonedirectory;","isw613_usr","secret");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 $_SESSION['nameS']= $_POST['name'] ?? "";
 $_SESSION['lastnameS']= $_POST['lastname'] ?? "";
 $_SESSION['officeS']= $_POST['office'] ?? "";
 $_SESSION['depS']= $_POST['department'] ?? "";

 try {
   $search = $db->prepare("SELECT * FROM phones_numbers INNER JOIN persons ON persons.id = phones_numbers.person_id INNER JOIN departments ON
     departments.id = persons.department_id INNER JOIN offices ON offices.id = departments.office_id WHERE persons.first_name like '%". $_SESSION['nameS'] ."%'
     AND offices.office_name LIKE '%". $_SESSION['officeS'] ."%' AND persons.last_name LIKE '%". $_SESSION['lastnameS'] ."%' AND departments.department_name LIKE '%". $_SESSION['depS'] ."%';");
   $search->execute(array());
  } catch (PDOException $e) {

  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Search</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="">AvanSoftware</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <?php  ?>
          <li class="nav-item active">
            <a class="nav-link" href="">Phone's number directory</a>
          </li>
        </ul>
        <div class="">
          <a class="btn btn-outline-warning btn-sm" href="/view/system/"> Back </a>
        </div>
      </div>
    </nav>
    <br><br>
    <div class="container">
      <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
          <form class="" action="/view/crud/search.php" method="post">
          <div class="row">
            <div class="col-md-3">
              <input type="text" id="name" class="form-control" name="name" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1">
            </div>
            <div class="col-md-3">
                <input type="text" id="lastname" class="form-control" name="lastname" placeholder="Lastname" aria-label="Lastname" aria-describedby="basic-addon1">
            </div>
            <div class="col-md-3">
              <input type="text" id="office" class="form-control" name="office" placeholder="Office" aria-label="Office" aria-describedby="basic-addon1">
            </div>
            <div class="col-md-3">
              <input type="text" id="department" class="form-control" name="department" placeholder="Department" aria-label="Department" aria-describedby="basic-addon1">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-9">

            </div>
            <div class="col-md-3">
              <div class="col-md-4">
                <button type="submit" class="btn btn-outline-warning">Search</button>
              </div>
            </div>
          </div>
          </form>
        </div>
        <div class="col-md-2">

        </div>

      </div>
      <br>

      <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
          <table class="table">
            <thead class="thead-inverse bg-dark" style="color: #fff;">
              <tr>
                <th>Type</th>
                <th>Phone Number</th>
                <th>Extension</th>
                <th>Name</th>
                <th>Lastname</th>
                <th>Office</th>
                <th>Department</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = $search->fetch(PDO::FETCH_ASSOC)) {?>
                  <tr>
                    <td><?php echo $row['type']; ?></td>
                    <td><?php echo $row['phone_number']; ?></td>
                    <td><?php echo $row['extension'] ?></td>
                    <td><?php echo $row['first_name'] ?></td>
                    <td><?php echo $row['last_name'] ?></td>
                    <td><?php echo $row['office_name'] ?></td>
                    <td><?php echo $row['department_name'] ?></td>
                  </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="col-md-2">

        </div>
      </div>
    </div>
  </body>
</html>
