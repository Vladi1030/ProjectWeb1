<?php
//crea una sesión o reanuda la actual basada en un identificador de sesión pasado mediante una petición GET o POST, o pasado mediante una cookie.
session_start();
//isset verifica si una variable fue declarada o es null
if(isset($_SESSION['usr_name'])!="") {
    //es usado para enviar encabezados HTTP sin formato
    //Recuerde que header() debe ser llamado antes de mostrar nada por pantalla, etiquetas HTML, líneas en blanco desde un fichero o desde PHP.
    header("Location: /view/system/");
}
?>

<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>AvanSoftware login</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
   </head>
   <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
         <a class="navbar-brand" href=""> AvanSoftware </a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
               <li class="nav-item active">
                  <a class="nav-link" href=""> Phone's number directory </a>
               </li>
               </li>
            </ul>
            <div class="">
               <a class="btn btn-outline-warning btn-sm" href="/"> Back </a>
            </div>
         </div>
      </nav>
      <br>
      <br>
      <div class="container">
        <div class="row">
          <?php if (isset($_SESSION['msg'])) {?>
            <div class="alert alert-warning" role="alert"><?php echo $_SESSION['msg']; ?></div>
          <?php } ?>
          <div class="col-md-4">

          </div>
          <div class="col-md-4">
            <div class="bg-dark card text-center">
              <div class="card-header">
                <hr class="btn-outline-warning">
              </div>
              <div class="card-body">
                <h5 style="color: #fff;">Welcome back</h5>
                <form class="login-form" action="/util/validation/login.php" method="POST">
                  <br>
                  <div class="input-group">
                    <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon1">@</span>
                    <input type="text" id="usr_name" class="form-control" name="usr_name" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                  </div>
                  <br>
                  <div class="input-group">
                    <span style="color: #fff;" class="input-group-addon bg-warning" id="basic-addon2">**</span>
                    <input type="password" id="password" class="form-control" name="password" placeholder="Password" aria-label="Pasword" aria-describedby="basic-addon2">
                  </div>
                  <br>
                  <button type="submit" class="btn btn-outline-warning">Log In</button>
                </form>
              </div>
              <div class="card-footer text-muted">
                  <hr class="btn-outline-warning">
              </div>
            </div>
          </div>
          <div class="col-md-4">

          </div>
        </div>
      </div>
   </body>
</html>
