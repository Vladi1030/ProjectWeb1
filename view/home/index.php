
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AvanSoftware</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body style="text-align: justify; text-justify: inter-word;">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="">AvanSoftware</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="">Phone's number directory</a>
            </li>
          </ul>
          <div class="">
            <a class="btn btn-outline-warning btn-sm" href="/view/login/"> Log In </a>
          </div>
        </div>
      </nav>
      <br><br><br>
      <div class="container">
        <div class="row">
          <div class="col-md-8 blog-main">
            <h2>What does this system?</h2>
            <p>December 02, 2017 by <a class="nav-brand" href="https://gitlab.com/Vladi1030">Vladimir Matarrita</a></p>
            <p>
            <hr class="btn-outline-warning">
            <p>
              <div class="row">
                <div class="col-md-6">
                  <img width="100%;" src="https://sitelabs.es/wp-content/uploads/2016/09/llamadas-mantenimiento-seo.jpg" alt="">
                </div>
                <div class="col-md-6">
                  <p>Your phone's number directory, you can access to the phone's number list of your company, but first you need to send us an email to add you as an user.</p>
                </div>
              </div>
            </p>
            </p>
          </div>
          <div class="col-md-3 blog-sidebar">
            <h3>Information</h3>
            <br>
            <hr class="btn-outline-warning">
            <p>
              If you are havnig troubles with this platform please, let us know.
              You can send us an email. Thanks
              <br>
              <br>
             Email: <a href="mailto:vmatarrita88@gmail.com">Vladimir Matarrita</a>
            </p>
          </div>
        </div>
      </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
