drop database if exists isw613_phonedirectory;
create database isw613_phonedirectory;

create user if not exists 'isw613_usr'@'localhost' identified by 'secret';
grant all privileges on isw613_phonedirectory.* to 'isw613_usr'@'localhost';
flush privileges;

-- create the tables
use isw613_phonedirectory;

create table users
( id int unsigned not null primary key auto_increment,
 full_name varchar(150) not null,
 username varchar(50) not null,
 password varchar(50) not null,
 role enum('admin', 'user') not null
) engine = innodb;

create table offices
( id int unsigned not null primary key auto_increment,
 office_name varchar(100) not null
) engine = innodb;

create table departments
( id int unsigned not null primary key auto_increment,
 department_name varchar(100) not null,
 office_id int unsigned not null,
 foreign key (office_id) references offices(id)
) engine = innodb;

create table persons
( id int unsigned not null primary key auto_increment,
 first_name varchar(150) not null,
 last_name varchar(50) not null,
 department_id int unsigned not null,
 foreign key (department_id) references departments(id)
) engine = innodb;

create table phones_numbers
( id int unsigned not null primary key auto_increment,
 type enum('work', 'mobile', 'home'),
 extension int,
 phone_number varchar(20) not null,
 annotation varchar(150),
 person_id int unsigned not null,
 foreign key (person_id) references persons(id)
) engine = innodb;

INSERT INTO users(full_name, username, password, role) VALUES('Main administrator','root', '7b24afc8bc80e548d66c4e7ff72171c5', 'admin');
