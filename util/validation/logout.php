<?php
  session_start();
  session_destroy();
  unset($_SESSION['usr_name']);
  unset($_SESSION['password']);
  unset($_SESSION['role']);
  unset($_SESSION['full_name']);
  unset($_SESSION['msg']);
  header("Location: /");
?>
