<?php
  //set validation error flag as false
  $error = false;

  session_start();

  $db = new PDO("mysql:host=localhost;dbname=isw613_phonedirectory;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $form = $_POST['form'];

  if ($form == "insert_user") {
    $_SESSION['form']="usr";
    $id = $_POST['user_id'];
    $name = $_POST['full_name'];
    $usrname = $_POST['usr_name'];
    $password = $_POST['password'];
    $cpassword = $_POST['ver_password'];
    $type =  $_POST['options'];

    //name can contain only alpha characters and space
    if(strlen($password) < 6) {
        $error = true;
        $_SESSION['msg'] = "Password must be minimum of 6 characters";
    }
    if($password != $cpassword) {
        $error = true;
        $_SESSION['msg'] = "Password and Confirm Password doesn't match";
    }
    if (!$error) {
      if ($id==0) {
        try {
        $stmt = $db->prepare("INSERT INTO users(full_name, username, password, role) VALUES(:name, :username, :password, :role)");;
        $stmt->execute(array(":name"=>$name,
                             ":username" =>$usrname,
                             ":password" => md5($password),
                             ":role" =>$type));
        $_SESSION['msg'] = "User added";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      } elseif ($id!=0) {
        try {
          //echo "UPDATE users SET full_name = '".$name."', username = '".$usrname."', password = '".md5($password)."', role = ".$type." WHERE id = ".$id.";";
          $stmt = $db->prepare("UPDATE users SET full_name = :name, username = :username, password = :password, role = :role WHERE id = :id;");
          $stmt->execute(array(":name"=>"$name",
                               ":username" =>"$usrname",
                               ":password" => md5($password),
                               ":role" =>"$type",
                             ":id"=>$id));
          $_SESSION['msg'] = "User updated";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      }
    }
  }elseif ($form == "insert_office") {
    $id = $_POST['office_id'];
    $office_name =  $_POST['office_name'];
    echo "office_id: ".  $_POST['office_id'];
    if($office_name == "") {
        $error = true;
        $_SESSION['msg'] = "Office name is required";
    }
    if (!$error) {
      if ($id==0) {
        try {
        $stmt = $db->prepare("INSERT INTO offices(office_name) VALUES(:office_name)");;
        $stmt->execute(array(":office_name"=>$office_name));
        $_SESSION['msg'] = "Office added";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      } elseif ($id!=0) {
        try {
        $stmt = $db->prepare("UPDATE offices SET office_name = :office_name WHERE id = :id;");;
        $stmt->execute(array(":office_name"=>"$office_name",
                              ":id"=>$id));
        $_SESSION['msg'] = "Office updated";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      }
    }
  }elseif ($form == "insert_department") {
    $id = $_POST['dep_id'];
    $deparment_name =  $_POST['department_name'];
    $office_id =  $_POST['office_id'];
    if($deparment_name == "") {
        $error = true;
        $_SESSION['msg'] = "Department name is required";
    }
    if (!$error) {
      if ($id=="0") {
        try {
          //echo "INSERT INTO departments(department_name, office_id) VALUES(".$deparment_name.", ".$office_id.");";
        $stmt = $db->prepare("INSERT INTO departments(department_name, office_id) VALUES(:department_name, :office_id);");;
        $stmt->execute(array(":office_id"=>$office_id,
                             ":department_name" =>$deparment_name));
        $_SESSION['msg'] = "Department added";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      } elseif ($id!=0) {
        try {
          $stmt = $db->prepare("UPDATE departments SET department_name = :department_name, office_id = :office_id WHERE id = :id;");;
          $stmt->execute(array(":office_id"=>$office_id,
                               ":department_name" =>$deparment_name,
                             ":id"=>$id));
          $_SESSION['msg'] = "Department updated";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      }
    }
  }elseif ($form == "insert_person") {
    $id = $_POST['per_id'];
    $first_name =  $_POST['first_name'];
    $last_name =  $_POST['last_name'];
    $deparment =  $_POST['department_id'];
    if($first_name == "") {
        $error = true;
        $_SESSION['msg'] = "Please type the name";
    }
    if($last_name == "") {
        $error = true;
        $_SESSION['msg'] = "Please type the lastname";
    }
    if (!$error) {
      if ($id=="0") {
        try {
          $stmt = $db->prepare("INSERT INTO persons(first_name, last_name, department_id) VALUES(:first_name, :last_name, :department_id)");;
          $stmt->execute(array(":first_name"=>$first_name,
                               ":last_name" =>$last_name,
                               ":department_id" => $deparment));
          $_SESSION['msg'] = "Person added";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      } elseif ($id!=0) {
        try {
          $stmt = $db->prepare("UPDATE persons SET first_name = :first_name, department_id = :department_id, last_name = :last_name WHERE id = :id;");;
          $stmt->execute(array(":first_name"=>$first_name,
                               ":last_name" =>$last_name,
                               ":department_id" => $deparment,
                             ":id"=>$id));
                               $_SESSION['msg'] = "Person updated";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      }
    }
  }elseif ($form == "insert_phone") {
    $id = $_POST['pho_id'];
    $type =  $_POST['type'];
    $extension =  $_POST['extension'];
    $phone =  $_POST['phone_number'];
    $annotation =  $_POST['annotation'];
    $person =  $_POST['person_id'];
    if($type == "") {
        $error = true;
        $_SESSION['msg'] = "Type is required";
    }
    if($phone  == "") {
        $error = true;
        $_SESSION['msg'] = "Please type the phone number";
    }
    if($person < 0 || $person==null) {
        $error = true;
        $_SESSION['msg'] = "Please chose the person";
    }
    if($extension == "") {
        $error = true;
        $_SESSION['msg'] = "Please type the extension";
    }
    if (!$error) {

      echo $id;
      if ($id==0) {
        try {
          $stmt = $db->prepare("INSERT INTO phones_numbers(type, extension, phone_number, annotation, person_id) VALUES(:type, :extension, :phone_number, :annotation, :person_id)");;
          $stmt->execute(array(":type"=>$type,
                               ":extension" =>$extension,
                               ":phone_number" => $phone,
                               ":annotation" => $annotation,
                               ":person_id" => $person,
                                ));
          $_SESSION['msg'] = "Phone number added";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      }elseif ($id!=0) {
        try {
          //echo "UPDATE phones_numbers SET type = ".$type.", extension = ".$extension.", phone_number = ".$phone.", annotation = annotation, person_id = person_id WHERE id = :id;";
          $stmt = $db->prepare("UPDATE phones_numbers SET type = :type, extension = :extension, phone_number = :phone_number, annotation = :annotation, person_id = :person_id WHERE id = :id;");;
          $stmt->execute(array(":type"=>$type,
                               ":extension" =>$extension,
                               ":phone_number" => $phone,
                               ":annotation" => $annotation,
                               ":person_id" => $person,
                               ":id" => $id));
          $_SESSION['msg'] = "Phone number updated";
        } catch (\Exception $e) {
          $_SESSION['msg'] = "Try again";
        }
      }
    }
  }
  header("Location: /view/system/");
?>
