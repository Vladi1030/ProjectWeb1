<?php
  $type = $_POST['crud_type'];
  $id = $_POST['id'];
  session_start();
  $db = new PDO("mysql:host=localhost;dbname=isw613_phonedirectory;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


  if ($type == "usr") {
    try {
       $stmt = $db->prepare("DELETE FROM users WHERE id = :id;");
       $stmt->execute(array(":id"=>"$id"));
       $_SESSION['msgd'] = "User removed";
     } catch (PDOException $e) {
         $_SESSION['msgd'] = "Error, there are other tables using this user";
     }
    header("Location: /view/crud/users.php");
  }elseif ($type == "per") {
    try {
      $stmt = $db->prepare("DELETE FROM persons WHERE id = :id;");
      $stmt->execute(array(":id"=>"$id"));
       $_SESSION['msgd'] = "Person removed";
     } catch (PDOException $e) {
         $_SESSION['msgd'] = "Error, there are other tables using this person";
     }
    header("Location: /view/crud/persons.php");
  }elseif ($type == "off") {
    try {
      $stmt = $db->prepare("DELETE FROM offices WHERE id = :id;");
      $stmt->execute(array(":id"=>"$id"));
       $_SESSION['msgd'] = "Office removed";
      header("Location: /view/crud/offices.php");
     } catch (PDOException $e) {
         $_SESSION['msgd'] = "Error, there are other tables using this office";
         header("Location: /view/crud/offices.php");
     }
  }elseif ($type == "dep") {
    try {
      $stmt = $db->prepare("DELETE FROM departments WHERE id = :id;");
      $stmt->execute(array(":id"=>"$id"));
       $_SESSION['msgd'] = "Department removed";
     } catch (PDOException $e) {
         $_SESSION['msgd'] = "Error, there are other tables using this department";
     }

    header("Location: /view/crud/department.php");
  }elseif ($type == "phone") {
    try {
      $stmt = $db->prepare("DELETE FROM phones_numbers WHERE id = :id;");
      $stmt->execute(array(":id"=>"$id"));
       $_SESSION['msgd'] = "Phone number removed";
     } catch (PDOException $e) {
         $_SESSION['msgd'] = "Error, there are other tables using this phone number";
     }

    header("Location: /view/crud/phones.php");
  }
?>
