<?php
  $type = $_POST['crud_type'];
  $id = $_POST['id'];

  $db = new PDO("mysql:host=localhost;dbname=isw613_phonedirectory;","isw613_usr","secret");
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  if ($type == "usr") {
    $stmt = $db->prepare("UPDATE FROM users WHERE id = :id;");
    $stmt->execute(array(":id"=>"$id"));
    header("Location: /view/crud/users.php");
  }elseif ($type == "per") {
    $stmt = $db->prepare("UPDATE FROM persons WHERE id = :id;");
    $stmt->execute(array(":id"=>"$id"));
    header("Location: /view/crud/persons.php");
  }elseif ($type == "off") {
    $stmt = $db->prepare("UPDATE FROM offices WHERE id = :id;");
    $stmt->execute(array(":id"=>"$id"));
    header("Location: /view/crud/offices.php");
  }elseif ($type == "dep") {
    $stmt = $db->prepare("UPDATE FROM departments WHERE id = :id;");
    $stmt->execute(array(":id"=>"$id"));
    header("Location: /view/crud/department.php");
  }
?>
